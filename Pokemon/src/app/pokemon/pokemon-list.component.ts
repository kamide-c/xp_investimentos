import { Component, OnInit } from "@angular/core";

import { Pokemon } from "./pokemon.model";
import { PokemonService } from "./pokemon-service";

@Component({
  selector: "all-list",
  moduleId: module.id,
  templateUrl: "./pokemon-list.component.html",
})
export class PokemonListComponent implements OnInit {
  pokemon: Pokemon[];
  page: number = 1;

  constructor(private pokemonService: PokemonService) { }

  ngOnInit(): void {
    this.pokemonService.list()
      .then((pokemon) => {
        this.pokemon = pokemon;
      });
  }
}